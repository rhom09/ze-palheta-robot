*** Settings ***
Documentation       Cadastro de clientes

Resource            ${EXECDIR}/resources/base.robot

Suite Setup          Login Session
Suite Teardown       Finish Session

*** Test Cases ***
Novo cliente
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     Bon Jovi        00000000001     Rua dos Bugs, 404       11999999999
    Quando faço a inclusão desse cliente    
    Então devo ver a notificação:       Cliente cadastrado com sucesso!

Cliente duplicado
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     Adrian Smith        00000000002     Rua dos Bugs, 403       11999999991
    Mas esse cpf já exixte no sistema
    Quando faço a inclusão desse cliente 
    Então devo ver a notificação de erro:       Este CPF já existe no sistema!

Campos Obrigatórios
    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     ${EMPTY}        ${EMPTY}     ${EMPTY}       ${EMPTY}
    Quando faço a inclusão desse cliente    
    Então devo ver mensagens informando que os campos do cadastro de clientes são obrigatórios

Nome é obrigatório
    [Tags]      required
    [Template]      Validação de Campos
    ${EMPTY}        11111111111     Rua dos Bugs, 404       11999999999     Nome é obrigatório

Cpf é obrigatório
    [Tags]      required
    [Template]      Validação de Campos
    Romilton Viana        ${EMPTY}     Rua dos Bugs, 404       11999999999     CPF é obrigatório

Endereço é obrigatório
    [Tags]      required
    [Template]      Validação de Campos
    Romilton Viana        11111111111     ${EMPTY}       11999999999     Endereço é obrigatório

Telefone é obrigatório
    [Tags]      required
    [Template]      Validação de Campos
    Romilton Viana        11111111111     Rua dos Bugs, 404       1199999999     Telefone inválido

Telefone incorreto
    [Tags]      required
    [Template]      Validação de Campos
    Romilton Viana        11111111111     Rua dos Bugs, 404       ${EMPTY}     Telefone é obrigatório    


*** Keywords ***
Validação de Campos
    [Arguments]         ${nome}     ${cpf}      ${endereco}      ${telefone}        ${saida}

    Dado que acesso o formulário de cadastro de clientes
    E que eu tenho o seguinte cliente:
    ...     ${nome}        ${cpf}     ${endereco}       ${telefone}
    Quando faço a inclusão desse cliente    
    Então devo ver texto:       ${saida}                     