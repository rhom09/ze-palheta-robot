*** Settings ***
Documentation       Login

Resource            ${EXECDIR}/resources/base.robot

Test Setup          Start Session
Test Teardown       Finish Session

*** Test Cases ***
Login do administrador
    Acesso a página Login
    Submeto minhas credenciais  admin@zepalheta.com.br  pwd123
    Devo ver a área logada                  