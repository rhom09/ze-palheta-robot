*** Settings ***
Documentation       Login Tentativa com Template

Resource            ${EXECDIR}/resources/base.robot

suite Setup          Start Session
suite Teardown       Finish Session

Test Template       Tentativa de login

*** Keywords ***
Tentativa de login
    [Arguments]     ${input_email}      ${input_senha}      ${output_mensagem}

    Acesso a página Login
    Submeto minhas credenciais  ${input_email}  ${input_senha}
    Devo ver um toaster com a mensagem  ${output_mensagem}

*** Test Cases ***
Senha incorreta             admin@zepalheta.com.br  abc123      Ocorreu um erro ao fazer login, cheque as credenciais.
Senha em branco             admin@zepalheta.com.br  ${EMPTY}    O campo senha é obrigatório!
Email incorreto             ze@palheta.com.br       pwd123      Ocorreu um erro ao fazer login, cheque as credenciais.
Email em branco             ${EMPTY}                123456      O campo email é obrigatório!
Email e senha em branco     ${EMPTY}                ${EMPTY}    Os campos email e senha não foram preenchidos         